# kotek-engine-deps-win32

Win32 deps folder for kotek-engine. All compiled dependencies are built with msvc compiler.

# Dependencies (already "offline" installed)

Offline installed libraries are libraries that install with command "cmake --install . --config YOUR_CMAKE_BUILD_TYPE".
It means we support only those plaforms that we develop on. 

Manually installed means that dependency was manually downloaded and was built and was installed with CMake. So if you want to have your own versions of dependencies that are built with different macros or compilers you need to make it on your own, because these dependencies are built and installed only for Windows development.

So if you see no in the field "manually installed" it means it's a header only dependency.

You need to remember that we don't change any source code of dependency that we use. It's really important, because some projects require the statement to not change the source code of project that you use.

| Dependency name | Version | Manually installed |
| ----------- | ----------- | ----------- |
| Boost | 1.77 | Yes |
| Bullet | 3.17 | Yes |
| CMake | 3.19.3 | By User on your local machine |
| DirectXMath | 3.16 | No |
| Eigen | 3.4.0 | No |
| FMT | 8.0.1 | Yes |
| glfw3 | 3.3.5 | Yes |
| glm | 0.9.9.8 | No |
| imgui | 1.85 (No docking) | No |
| KTX-Software | 4.0.0 | Yes |
| mimalloc | 1.7 | Yes |
| Tracy | 0.7.8 | No |
| shaderc | 1.5.5 | Yes |
| SPIRV-Reflect | Not stated | No |
| oneTBB | 2021.6.0 | Yes |
| Visual Leak Detector | 2.5.1 | No (Windows only) |
| wxWidgets | 3.1.5  | Yes |

# License status 

Необходимо понимать что лицензирование текущего проекта при обновлении его же зависимостей может повлечь к состоянию когда текущая лицензия этого проекта будет несовместимой с новой лицензией какой-либо зависимости. Поэтому здесь приводится список всех лицензий всех зависимостей, и текущая лицензия которая используется

kotek-engine's (not this repository) license is Apache 2.0

ATTENTION: We appreciate your help in case when you show us where we're wrong and have incompatibale license with what we have

| Dependency's name | License | Compatibility with project's license | Explanation | 
| ----------- | ----------- | ----------- | ----------- |
| Boost | Boost Software license - Version 1.0 | Yes |  |
| Bullet | Zlib license | Yes |  |
| DirectXMath | MIT license | Yes |  |
| Eigen | MPL 2.0 license | Yes |  |
| FMT | MIT license | Yes | Yes |
| glfw3 | Zlib license | Yes |  |
| glm | MIT license | Yes |  |
| imgui | MIT license | Yes |  |
| KTX-Software | Apache-2.0 license | The same as project's license |  |
| mimalloc | MIT license | Yes | Yes |
| Tracy | 3-clause BSD license | Yes |  |
| shaderc | Apache-2.0 license | The same as project's license |  |
| SPIRV-Reflect | Apache-2.0 license | The same as project's license |  |
| oneTBB | Apache-2.0 license | The same as project's license |  |
| Visual Leak Detector | LGPL-2.1 license | Yes | We don't change the code and use only library for linking only and use dll file for 'working with library' |
| wxWidgets | wxWidgets license  | Yes | We don't alter the source code of WxWidgets library and use only for linking their pre-build static and dynamic libraries. As the result user gets only dynamic libraries after building the project. |